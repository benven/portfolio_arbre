
if (window.matchMedia("(min-width: 1000px)").matches) {
    /* CODE POUR LES ECRANS SUPERIEURS A 1000PX */
    const flightPath = {
        curviness: 1.25,
        autoRotate: true,
        values: [
            {x:100, y: 0}, 
            {x:200, y: 100}, 
            {x:300, y: 200}, 
            {x:400, y: 300}, 
            {x:410, y: 400}, 
            {x:420, y: 500}, 
            {x:430, y: 600}, 
            {x:420, y: 700}, 
            {x:410, y: 800}, 
            {x:400, y: 900}, 
            {x:410, y: 1000}, 
            {x:420, y: 1050}, 
            {x:430, y: 1100}, 
            {x:440, y: 1200}, 
            {x:420, y: 1400}, 
            {x:430, y: 1500}, 
            {x:420, y: 1600}, 
            {x:415, y: 1700}, 
            {x:410, y: 1800}, 
            {x:400, y: 1900}, 
            {x:390, y: 2000}, 
            {x:380, y: 2100}, 
            {x:440, y: 2200}, 
            {x:430, y: 2300}, 
            {x:420, y: 2400}, 
            {x:410, y: 2500}, 
            {x:440, y: 2600}, 
            {x:430, y: 2700}, 
            {x:420, y: 2800}, 
            {x:410, y: 2900}, 
            {x:430, y: 3000}, 
            {x:420, y: 3100}, 
            {x:410, y: 3200}, 
            {x:400, y: 4000}, 
            {x:420, y: 4100}, 
            {x:410, y: 4200}, 
            {x:400, y: 4500}, 
            {x:400, y: 4600}, 
            {x:420, y: 4700}, 
            {x:410, y: 4800}, 
            {x:400, y: 4900}, 
            {x:450, y: -window.innerWidth}, 
        ] 
    }
    
    const tween = new TimelineLite();
    
    
    tween.add(
        TweenLite.to('.pollen', 1, {
            bezier : flightPath,
            ease : Power1.easeInOut
        })
    );
    
    const _controller = new ScrollMagic.Controller();
    
    const scene = new ScrollMagic.Scene({
      triggerElement: ".animation",
      duration: 4000, //scroll time here
      triggerHook: 0,
    })
      .setTween(tween)
      
      .setPin(".animation")
      .addTo(_controller);
  } else { //CODE POUR LES ECRANS < 1000PX

       const flightPath = {
        curviness: 1.25,
        autoRotate: true,
        values: [
            {x:100, y: 0}, 
            {x:200, y: 100}, 
            {x:200, y: 200}, 
            {x:200, y: 300}, 
            {x:210, y: 400}, 
            {x:220, y: 500}, 
            {x:230, y: 600}, 
            {x:220, y: 700}, 
            {x:210, y: 800}, 
            {x:200, y: 900}, 
            {x:210, y: 1000}, 
            {x:220, y: 1050}, 
            {x:230, y: 1100}, 
            {x:240, y: 1200}, 
            {x:220, y: 1400}, 
            {x:230, y: 1500}, 
            {x:220, y: 1600}, 
            {x:215, y: 1700}, 
            {x:210, y: 1800}, 
            {x:200, y: 1900}, 
            {x:290, y: 2000}, 
            {x:280, y: 2100}, 
            {x:240, y: 2200}, 
            {x:230, y: 2300}, 
            {x:220, y: 2400}, 
            {x:210, y: 2500}, 
            {x:240, y: 2600}, 
            {x:230, y: 2700}, 
            {x:220, y: 2800}, 
            {x:210, y: 2900}, 
            {x:230, y: 3000}, 
            {x:220, y: 3100}, 
            {x:210, y: 3200}, 
            {x:200, y: 4000}, 
            {x:220, y: 4100}, 
            {x:210, y: 4200}, 
            {x:200, y: 4500}, 
            {x:200, y: 4600}, 
            {x:220, y: 4700}, 
            {x:210, y: 4800}, 
            {x:200, y: 4900}, 
            {x:250, y: -window.innerWidth}, 
        ] 
    }
    
    const tween = new TimelineLite();
    
    
    tween.add(
        TweenLite.to('.pollen', 1, {
            bezier : flightPath,
            ease : Power1.easeInOut
        })
    );
    
    const _controller = new ScrollMagic.Controller();
    
    const scene = new ScrollMagic.Scene({
      triggerElement: ".animation",
      duration: 2500, //scroll time here
      triggerHook: 0,
    })
      .setTween(tween)
      
      .setPin(".animation")
      .addTo(_controller);
    
  }


  